# Minocular

The template for shadow projects created by the Labocular repository. It contains the mining pipeline to create offline artifacts using Binocular. Before the mining pipeline can be executed, its customised Docker image has to be built. This can be done by executing the `build_minocular_image` job in the Visocular repository.

Note: variables defined in `.gitlab-ci.yml` have to be lowercase!
