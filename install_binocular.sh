# Clone Binocular repository
echo
echo "Installing Binocular"
echo

git clone --single-branch --branch develop https://github.com/INSO-TUWien/Binocular.git /Binocular
cd /Binocular

echo
echo "Installing Dependencies"
echo
npm install --quiet --legacy-peer-deps
#TODO: remove; at the moment just for compatibility
npm install request --save

