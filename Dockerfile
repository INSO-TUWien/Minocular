FROM node:18

COPY install_binocular.sh install_binocular.sh

RUN apt-get update && apt-get -y install wget
RUN wget -nv https://download.arangodb.com/arangodb37/Community/Linux/arangodb3-client-linux-3.7.11.tar.gz
RUN tar -xf arangodb3-client-linux-3.7.11.tar.gz

RUN ./install_binocular.sh
