# name of main javascript file and executable (and prefix for databases)
APP_NAME="binocular"
BINOCULARRC=".binocularrc"
BINOCULAR_INSTALL_DIR="/Binocular"
echo "#################### Printing environment variables ####################"
echo "+ APP_NAME:                 "${APP_NAME}
echo "+ BINOCULARRC:              "${BINOCULARRC}
echo "+ BINOCULAR_INSTALL_DIR:    "${BINOCULAR_INSTALL_DIR}
echo "+ CI_BUILDS_DIR:            "${CI_BUILDS_DIR}
echo "+ CI_PROJECT_PATH:          "${CI_PROJECT_PATH}
echo "+ CI_SERVER_URL:            "${CI_SERVER_URL}
echo "+ REPO_HOST_URL:            "${REPO_HOST_URL}
echo "+ GROUP_NAME_LOWERCASE:     "${GROUP_NAME_LOWERCASE}
echo "+ ORIGIN_GITLAB_PROJECT_ID: "${ORIGIN_GITLAB_PROJECT_ID}
echo "+ ORIGIN_REPO_NAME:         "${ORIGIN_REPO_NAME}
echo "+ ORIGIN_REPO_NAMESPACE:    "${ORIGIN_REPO_NAMESPACE}
echo "########################################################################"

if [ -z "${REPO_HOST_URL}" ]; then
  # Use CI_SERVER_URL if REPO_HOST_URL is not set
  repo_url="${CI_SERVER_URL}"
  echo "No value set for REPO_HOST_URL. Setting it to CI_SERVER_URL: ${repo_url}"
else
  repo_url="${REPO_HOST_URL}"
  echo "Using provided (env) REPO_HOST_URL: ${repo_url}"
fi

# Extract protocol from URL
protocol=$(echo "${repo_url}" | awk -F:// '{print $1}')
echo "Protocol:" ${protocol}

# Store the remaining part of the URL
repo_base_url=$(echo "$repo_url" | awk -F:// '{$1=""; print substr($0,2)}')
echo "Base URL:" $repo_base_url
export REPO_HOST=$repo_base_url # should be removed in the future, needed for compatibility
echo "REPO_HOST:" ${REPO_HOST}


REPO_URL_NO_PROTOCOL="${repo_base_url}/$ORIGIN_REPO_NAMESPACE/$ORIGIN_REPO_NAME"
# REPO_URL="${protocol}://${remaining_url}/$ORIGIN_REPO_NAMESPACE/$ORIGIN_REPO_NAME"
echo ${REPO_URL_NO_PROTOCOL}

echo Clone target repository
echo
echo "Cloning target repository (${REPO_URL_NO_PROTOCOL})"
echo
if git clone "${protocol}://${GITLAB_USER_LOGIN}:${GITLAB_TOKEN}@${REPO_URL_NO_PROTOCOL}" "/${ORIGIN_REPO_NAME}"; then
  echo "Cloning successful."
  # Continue with the rest of your script here
else
  echo "Cloning failed. Exiting."
  exit 1  # Exit with a non-zero status code to indicate failure
fi

# Print ls -lh output
# echo "###########################################################################"
# echo "Contents of the current directory:"
# ls -lh .

# echo "###########################################################################"
# echo "Contents of the shadow repo directory:"
# ls -lh /${ORIGIN_REPO_NAME}

# echo "###########################################################################"
# echo "Contents of the / directory:"
# ls -lh /

# echo "###########################################################################"
# echo "Contents of the /Binocular directory:"
# ls -lh /Binocular

echo
echo "Creating .binocularrc"
echo


cd /${ORIGIN_REPO_NAME}

echo "{" >> $BINOCULARRC
echo " \"gitlab\": { \"url\": \"${protocol}://$REPO_HOST\", \"project\": \"$ORIGIN_REPO_NAMESPACE/$ORIGIN_REPO_NAME\", \"token\": \"$GITLAB_TOKEN\"}," >> $BINOCULARRC
echo " \"arango\": { \"host\": \"arangodb\", \"port\": \"8529\", \"user\": \"root\", \"password\":\"$ARANGO_ROOT_PASSWORD\"}," >> $BINOCULARRC
echo " \"indexers\": { \"its\": \"gitlab\", \"ci\": \"gitlab\"}" >> $BINOCULARRC
echo "}" >> $BINOCULARRC

echo
echo ".bincocularrc was created as follows:"
cat $BINOCULARRC
echo

echo
echo "Creating gitlab config json"
echo

cd ${BINOCULAR_INSTALL_DIR}/ui/config
rm "gitlab.json"
echo "{" >> "gitlab.json"
echo "\"server\": \"${protocol}://$REPO_HOST/\", \"projectId\": \"$ORIGIN_GITLAB_PROJECT_ID\"" >> "gitlab.json"
echo "}" >> "gitlab.json"

echo
echo "gitlab config json was created as follows:"
cat "gitlab.json"
echo




# mine repo, ITS and CI
echo
echo "Mining repository"
echo

cd /Binocular
node $APP_NAME.js --no-open --no-server ./../$ORIGIN_REPO_NAME

# bundle JSON exports with Binocular UI to create offline artifact
echo
echo "Creating offline artifact from ArangoDB exports"
echo

npm run build


cd /$CI_BUILDS_DIR
cd repo
cd $CI_PROJECT_PATH
mkdir dist
cd dist
cp ${BINOCULAR_INSTALL_DIR}/dist/index.html .
cp -R ${BINOCULAR_INSTALL_DIR}/dist/assets .
cp -R ${BINOCULAR_INSTALL_DIR}/ui/db_export .

echo "#########################################################################################################"
echo "Contents of the '$(pwd)' directory:"
ls -lh .
echo "#########################################################################################################"

echo
echo "Finished"
echo

